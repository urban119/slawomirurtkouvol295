﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    Rigidbody rig;
    Vector3 mouse_position;
    Vector2 position = new Vector2(0f, 0f);
    private bool running = false;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //uruchom ruch kulek, jezeli lewy przycisk myszki
        if (Input.GetButtonDown("Fire1"))
        {
            running = true;           
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            StartCoroutine(Wait());
        }

        if (running)
        {
            mouse_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            position = Vector2.Lerp(transform.position, mouse_position, 0.05f);
            rig.MovePosition(position);
        }
    }

    //koniec ruchu
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        running = false;
    }
}
