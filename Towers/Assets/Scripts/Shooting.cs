﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform fireTarget; //kierunek strzalu
    public GameObject bullet;
    public GameObject tower;

    // Start is called before the first frame update
    void Start()
    {
        //strzal
        StartCoroutine(Shoot());
    }

    // Update is called once per frame
    void Update()
    {   

    }

    //strzal
    void ShootBullet()
    {
        Instantiate(bullet, fireTarget.position, fireTarget.rotation);
    }
    
    //Zresp nową wieżę
    void SpawnNewTower()
    {
        Instantiate(tower, bullet.transform.position, bullet.transform.rotation);
    }

    //strzal co x czas
    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(2.5f);
        ShootBullet();
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2.5f);
        SpawnNewTower();
        StartCoroutine(Shoot());
    }



}
