﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f; //predkosc pocisku
    public Rigidbody2D rig;

    // Start is called before the first frame update
    void Start()
    {
        rig.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
